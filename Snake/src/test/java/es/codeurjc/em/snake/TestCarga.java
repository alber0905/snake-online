package es.codeurjc.em.snake;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;

public class TestCarga {
	
	@BeforeClass
	public static void startServer(){
		Application.main(new String[]{ "--server.port=9000" });
	}
	
	@Test
	public void testCarga() throws Exception {

		//Variables que guardan el indice de la sala
	   	AtomicInteger sala = new AtomicInteger();
	   	AtomicInteger sala2 = new AtomicInteger();
	   	
	   	
	   	CountDownLatch latch = new CountDownLatch(10);
	   	CountDownLatch latch2 = new CountDownLatch(20);
	   	
	   	ConcurrentHashMap <WebSocketClient,Integer> Clientes = new ConcurrentHashMap <WebSocketClient,Integer>();
	   	
	   	for(int i=0; i<10;i++) {
	   		Clientes.put(new WebSocketClient(), i);
	   	}
	           

	  //gestionamos la recepcion de mensajes
      for(WebSocketClient c : Clientes.keySet()){
          c.onMessage((session, msg) ->{
                  if(msg.contains("update")){
                   //System.out.println("Actualizacion: "+msg);
                  }else if (msg.contains("join")){
                       System.out.println("Entrando sala "+msg);
                  }else if(msg.contains("startGame")) {
               	   System.out.println("Comienza el juego "+msg);
                  }
                  
          });
          c.onClose((session, satatus)->{
          	latch.countDown();
          	latch2.countDown();
          });
          
      } 
      
      //Para cada Cliente
      for(WebSocketClient c : Clientes.keySet()){
   	  
		new Thread(()->{
		    try {
		    //Conectamos
		     c.connect("ws://127.0.0.1:9000/snake");
		     
		     //Comprobamos que el nick no existe
		   	 System.out.println(Unirest.post("http://127.0.0.1:9000/createPlayer").header("Content-Type", "application/json").body("{\"name\":\"Paquito"+Thread.currentThread().getName()+"\",\"difficulty\":0,\"gameMode\":0}").asJson().getBody());
		   	 
		   	 //Creamos una sala
		   	 HttpResponse<JsonNode> request = Unirest.post("http://127.0.0.1:9000/createRoom").header("Content-Type", "application/json").body("{\"name\":\"PruebaSala"+Thread.currentThread().getName()+"\",\"difficulty\":0,\"gameMode\":0}").asJson();
		   	 System.out.println(request.getBody());
		   	 JSONObject object = request.getBody().getObject();
		   	 int i = (int)object.get("id");
		   	 
		   	 //Nos conectamos a la sala
		   	 c.sendMessage("{\"instruction\": \"joinroom\", \"room\":"+i+", \"name\":\"Paquito"+Thread.currentThread().getName()+"\"}')");
		   	 Thread.sleep(2000);
		   	 
		   	 //Nos desconectamos
		   	 c.disconnect();
		    } catch (Exception e) {
		        System.out.println(e);
		    }
		}).start();   
      }
      //Esperamos a que todos salgan de su sala.
      latch.await();
	
      //Para cada cliente
      for (WebSocketClient c : Clientes.keySet()) {
		new Thread(()->{
            try {
            //Conectamos
             c.connect("ws://127.0.0.1:9000/snake");
             
             //Si es el cliente 0
             if(Clientes.get(c) == 0) {
            	 //Creamos la salaA y nos conectamos
            	 System.out.println(Unirest.post("http://127.0.0.1:9000/createPlayer").header("Content-Type", "application/json").body("{\"name\":\"Paquito"+Thread.currentThread().getName()+"\",\"difficulty\":0,\"gameMode\":0}").asJson().getBody());
               	 HttpResponse<JsonNode> request = Unirest.post("http://127.0.0.1:9000/createRoom").header("Content-Type", "application/json").body("{\"name\":\"SalaA\",\"difficulty\":0,\"gameMode\":0}").asJson();
        	   	 System.out.println(request.getBody());
        	   	 JSONObject object = request.getBody().getObject();
        	   	 sala.set((int)object.get("id"));
        	   	 c.sendMessage("{\"instruction\": \"joinroom\", \"room\":"+sala.get()+", \"name\":\"Paquito"+Thread.currentThread().getName()+"\"}')");
             }
             //Si es el cliente 5
             else if(Clientes.get(c) == 5) {
            	//Creamos la salaB y nos conectamos
            	 System.out.println(Unirest.post("http://127.0.0.1:9000/createPlayer").header("Content-Type", "application/json").body("{\"name\":\"Paquito"+Thread.currentThread().getName()+"\",\"difficulty\":0,\"gameMode\":0}").asJson().getBody());
               	 HttpResponse<JsonNode> request = Unirest.post("http://127.0.0.1:9000/createRoom").header("Content-Type", "application/json").body("{\"name\":\"SalaB\",\"difficulty\":0,\"gameMode\":0}").asJson();
        	   	 System.out.println(request.getBody());
        	   	 JSONObject object = request.getBody().getObject();
        	   	 sala2.set((int)object.get("id"));
        	   	 c.sendMessage("{\"instruction\": \"joinroom\", \"room\":"+sala2.get()+", \"name\":\"Paquito"+Thread.currentThread().getName()+"\"}')");
             }
             //Si son los clientes 1,2,3,4
             else if(Clientes.get(c)<5) {
            	 Thread.sleep(1000);
            	 //Nos conectamos a la salaA
            	 c.sendMessage("{\"instruction\": \"joinroom\", \"room\":"+sala.get()+", \"name\":\"Paquito"+Thread.currentThread().getName()+"\"}')");
             }
             //Si son los clientes 6,7,8,9
             else {
            	 Thread.sleep(1000);
            	//Nos conectamos a la salaB
            	 c.sendMessage("{\"instruction\": \"joinroom\", \"room\":"+sala2.get()+", \"name\":\"Paquito"+Thread.currentThread().getName()+"\"}')");
             }
           	 Thread.sleep(10000);
           	 //Desconectamos los clientes
           	 c.disconnect();
            } catch (Exception e) {
            	System.out.println(e);
            }
        }).start(); 
	}
      //El programa principal continua cuando todos los clientes se han desconectado
	latch2.await();
}
}

