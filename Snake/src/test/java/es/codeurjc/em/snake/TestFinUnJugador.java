package es.codeurjc.em.snake;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;

public class TestFinUnJugador {
/*
 * El objetivo de este test es el de generar dos clientes. El primero espera a que el segundo entre en la sala, y una vez lo ha hecho, se desconecta.
 * Como el segundo se actualiza, entonces deberá terminar la partida, por lo que entonces se desbloquea el hilo principal
 */
	@BeforeClass
	public static void startServer(){
		Application.main(new String[]{ "--server.port=9000" });
	}
	

	@Test
	 public void testTerminarPartida() throws Exception {

 	AtomicInteger sala = new AtomicInteger();
 	//Un countDown para el hilo principal y otro para esperar a que haya al menos dos jugadores en la sala
 	CountDownLatch latch = new CountDownLatch(2);
 	CountDownLatch esperarMinimo = new CountDownLatch(2);
         
 	HttpResponse<JsonNode> request = Unirest.post("http://127.0.0.1:9000/createRoom").header("Content-Type", "application/json").body("{\"name\":\"PruebaSala\",\"difficulty\":0,\"gameMode\":0}").asJson();
 	System.out.println(request.getBody());
 	JSONObject object = request.getBody().getObject();
 	sala.set((int)object.get("id"));

 	//Creamos dos sockets
 	WebSocketClient[] clientes = new WebSocketClient[2];
 	//Imprimimos los mensajes
    for(int i = 0; i<2; i++){
        WebSocketClient c = new WebSocketClient();
        //Impresión de los mensajes cuando:
        c.onMessage((session, msg) ->{
                if(msg.contains("update")){
                //Entra en la sala
                }else if (msg.contains("join")){
                     System.out.println("JOIN : "+msg);
                     esperarMinimo.countDown();
                //Empieza la partida
                }else if(msg.contains("startGame")) {
             	   System.out.println("START : "+msg);
             	//Abandona la sala
                }else if(msg.contains("leave")){
                	System.out.println("Jugador Desconectado");
                	latch.countDown();
                //Termina la partida
                }else if(msg.contains("finish")) {
              	  System.out.println("Partida Terminada : "+msg);
                }
        });
        //Cuando se desconecta un cliente, envia el mensaje
        c.onClose((session, satatus)->{
        	System.out.println("Se ha cerrado la sesión de : " + session.getId());
        	latch.countDown();
        });
        c.connect("ws://127.0.0.1:9000/snake");
        clientes[i] = c;
        
    } 
    

  	  //Se crea un primer hilo que espera a un segundo para comenzar la partida y desconectarse
       new Thread(()->{
           try {
          	 System.out.println(Unirest.post("http://127.0.0.1:9000/createPlayer").header("Content-Type", "application/json").body("{\"name\":\"Paquito"+Thread.currentThread().getName()+"\",\"difficulty\":0,\"gameMode\":0}").asJson().getBody());
          	clientes[0].sendMessage("{\"instruction\": \"joinroom\", \"room\":"+sala.get()+", \"name\":\"Paquito"+Thread.currentThread().getName()+"\"}')");
          	System.out.println(Thread.currentThread().getName()+" está esperando");
          	 esperarMinimo.await();
          	System.out.println(Unirest.get("http://127.0.0.1:9000/start/"+sala.get()).header("Content-Type", "application/json").asString());
          	 System.out.println("Desconectando: " +Thread.currentThread().getName());
          	clientes[0].disconnect();
           } catch (Exception ex) {
               System.out.println(ex);
           }
       }).start();  
       
       //Cuando se ha unido a la sala, empieza la partida y se desconecta el hilo anterior. Así se comprueba si la partida termina
       new Thread(()->{
           try {
          	 System.out.println(Unirest.post("http://127.0.0.1:9000/createPlayer").header("Content-Type", "application/json").body("{\"name\":\"Paquito"+Thread.currentThread().getName()+"\",\"difficulty\":0,\"gameMode\":0}").asJson().getBody());
          	clientes[1].sendMessage("{\"instruction\": \"joinroom\", \"room\":"+sala.get()+", \"name\":\"Paquito"+Thread.currentThread().getName()+"\"}')");
          	System.out.println(Thread.currentThread().getName()+" está esperando");
           } catch (Exception ex) {
        	   System.out.println(ex);
           }
       }).start(); 
       
     
     latch.await();
     System.out.println("Test finalizado");

	}


}
