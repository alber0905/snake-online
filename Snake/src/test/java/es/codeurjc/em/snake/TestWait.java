package es.codeurjc.em.snake;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;


import java.io.IOException;
import java.net.URI;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.websocket.Session;


import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.support.HttpRequestWrapper;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;


public class TestWait {

	@BeforeClass
	public static void startServer(){
		Application.main(new String[]{ "--server.port=9000" });
	}
	
	@Test
	public void testEspera() throws Exception {
    	AtomicInteger sala = new AtomicInteger();
    	AtomicBoolean close = new AtomicBoolean(false);
    	CountDownLatch latch = new CountDownLatch(1);
    	CountDownLatch esperarConexion = new CountDownLatch(5);
    	//Creamos la room de antemano
    	HttpResponse<JsonNode> request = Unirest.post("http://127.0.0.1:9000/createRoom").header("Content-Type", "application/json").body("{\"name\":\"PruebaSala\",\"difficulty\":0,\"gameMode\":0}").asJson();
    	System.out.println(request.getBody());
    	JSONObject object = request.getBody().getObject();
    	sala.set((int)object.get("id"));
    	
    	
    	WebSocketClient[] Sockets = new WebSocketClient[5];
    	//Creamos los 5 sockets
        for(int i = 0; i<5; i++){
            WebSocketClient c = new WebSocketClient();
            c.onMessage((session, msg) ->{
                    if(msg.contains("update")){
                    	//Descomentar esto si quisieramos ver los updates por algun motivo inexplicable
                     //System.out.println("Mensaje de update : "+msg);                     
                    }else if (msg.contains("join")){
                         System.out.println("Se ha unido un jugador : "+msg);
                         if(close.get()) {
                        	 latch.countDown();
                         }
                    }else if(msg.contains("startGame")) {
                 	   System.out.println("Comienzo de partida : "+msg);
                    }
                    else if(msg.contains("endGame")){
                        try {
                            c.disconnect();  
                        } catch (IOException ex) {
                            Logger.getLogger(SnakeTest.class.getName()).log(Level.SEVERE, null, ex);
                        }
                          }
            });
            c.onClose((session, satatus)->{
            	System.out.println("Sesión cerrada : " + session.getId());
            });
            c.connect("ws://127.0.0.1:9000/snake");
            Sockets[i] = c;
            
        }    

        WebSocketClient ultimo = Sockets[4];
        //Lanzamos el hilo que va a desconectar a un jugador
        new Thread(()->{
        	try {
        		//Esperamos a que se hayan unido los 5        		
        		esperarConexion.await();
        		//Los sleep es para poder apreciar bien por la consola que hay uno esperando
        		Thread.sleep(2000);
        		//Desconectamos a un jugador
        		close.set(true);
               	ultimo.disconnect();               	
               	//Con este sleep vemos que entra el 5
               	Thread.sleep(200);
               	//Dejamos que acabe el programa
                } catch (Exception ex) {
                    Logger.getLogger(SnakeTest.class.getName()).log(Level.SEVERE, null, ex);
                }
    	}).start();
        for(WebSocketClient c : Sockets){      	  
        	//Lanzamos 5 hilos que traten de conectarse
            new Thread(()->{
                try {
               	System.out.println(Unirest.post("http://127.0.0.1:9000/createPlayer").header("Content-Type", "application/json").body("{\"name\":\"Paquito"+Thread.currentThread().getName()+"\",\"difficulty\":0,\"gameMode\":0}").asJson().getBody());
               	c.sendMessage("{\"instruction\": \"joinroom\", \"room\":"+sala.get()+", \"name\":\"Paquito"+Thread.currentThread().getName()+"\"}')");
               	esperarConexion.countDown();
                } catch (Exception ex) {
                    Logger.getLogger(SnakeTest.class.getName()).log(Level.SEVERE, null, ex);
                }
            }).start(); 
          }
    		//Esperamos a que acaben los hilos que nos interesan
          latch.await();    	
	}


}
