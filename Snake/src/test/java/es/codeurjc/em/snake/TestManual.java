package es.codeurjc.em.snake;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;


import java.io.IOException;
import java.net.URI;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.websocket.Session;


import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.support.HttpRequestWrapper;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;

public class TestManual {
	@BeforeClass
	public static void startServer(){
		Application.main(new String[]{ "--server.port=9000" });
	}
	
	@Test
	 public void testJoinManualGame() throws Exception {

 	AtomicInteger sala = new AtomicInteger();
 	CountDownLatch latch = new CountDownLatch(2);
 	CountDownLatch ActivateButton = new CountDownLatch(1);
 	
    //Creamos la sala     
 	HttpResponse<JsonNode> request = Unirest.post("http://127.0.0.1:9000/createRoom").header("Content-Type", "application/json").body("{\"name\":\"PruebaSala\",\"difficulty\":0,\"gameMode\":0}").asJson();
 	System.out.println(request.getBody());
 	JSONObject object = request.getBody().getObject();
 	sala.set((int)object.get("id"));

 	//Manejamos la recepcion de mensajes
 	WebSocketClient[] Clientes = new WebSocketClient[2];
    for(int i = 0; i<2; i++){
        WebSocketClient c = new WebSocketClient();
        c.onMessage((session, msg) ->{
                if(msg.contains("update")){
                 System.out.println("Movimiento : "+msg);
                 latch.countDown();
                }else if (msg.contains("join")){
                     System.out.println("Entrando sala : "+msg);
                }else if(msg.contains("startGame")) {
             	   System.out.println("Empieza partida : "+msg);
                }else if(msg.contains("showStartButton")) {
              	  System.out.println("Botón empezar partida activado : "+msg);
              	  ActivateButton.countDown();
                }
                
        });
        c.connect("ws://127.0.0.1:9000/snake");
        Clientes[i] = c;
        
    } 
    

 	  //Creamos un thread que se conecta a la sala
      new Thread(()->{
          try {
        	 //Comprueba si el nombre se puede
         	 System.out.println(Unirest.post("http://127.0.0.1:9000/createPlayer").header("Content-Type", "application/json").body("{\"name\":\"Paquito"+Thread.currentThread().getName()+"\",\"difficulty\":0,\"gameMode\":0}").asJson().getBody());
         	 //Se conecta a la sala
         	 Clientes[0].sendMessage("{\"instruction\": \"joinroom\", \"room\":"+sala.get()+", \"name\":\"Paquito"+Thread.currentThread().getName()+"\"}')");
         	 //Espera hasta que se conecte el otro y le salga el botón de empezar partida
         	 ActivateButton.await();
         	 //Presiona el botñon start
         	System.out.println(Unirest.get("http://127.0.0.1:9000/start/"+sala.get()).header("Content-Type", "application/json").asString());
          } catch (Exception e) {
        	  System.out.println(e);
          }
      }).start();
      
      //Este thread es el segundo jugador que se conecta a la sala
      new Thread(()->{
          try {
         	 System.out.println(Unirest.post("http://127.0.0.1:9000/createPlayer").header("Content-Type", "application/json").body("{\"name\":\"Paquito"+Thread.currentThread().getName()+"\",\"difficulty\":0,\"gameMode\":0}").asJson().getBody());
         	Clientes[1].sendMessage("{\"instruction\": \"joinroom\", \"room\":"+sala.get()+", \"name\":\"Paquito"+Thread.currentThread().getName()+"\"}')");
         	 
          } catch (Exception e) {
        	  System.out.println(e);
          }
      }).start();
      
      
    
    
    
    //Espera a que los dos jugadores empiecen la partida
    latch.await();
    

	}
}
