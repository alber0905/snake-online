package es.codeurjc.em.snake;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;


import java.io.IOException;
import java.net.URI;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.websocket.Session;


import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.support.HttpRequestWrapper;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;

public class TestAutomatic {
	@BeforeClass
	public static void startServer(){
		Application.main(new String[]{ "--server.port=9000" });
	}
	
	@Test
	 public void testJoinAutomaticGame() throws Exception {

   	AtomicInteger sala = new AtomicInteger();
   	CountDownLatch latch = new CountDownLatch(4);
    
   	//Creamos sala
   	HttpResponse<JsonNode> request = Unirest.post("http://127.0.0.1:9000/createRoom").header("Content-Type", "application/json").body("{\"name\":\"PruebaSala\",\"difficulty\":0,\"gameMode\":0}").asJson();
   	System.out.println(request.getBody());
   	JSONObject object = request.getBody().getObject();
   	sala.set((int)object.get("id"));

   	
   	WebSocketClient[] Clientes = new WebSocketClient[4];
      for(int i = 0; i<4; i++){
          WebSocketClient c = new WebSocketClient();
          c.onMessage((session, msg) ->{
                  if(msg.contains("update")){
                	  System.out.println("Actualización : "+msg);
                	  latch.countDown();
                  }else if (msg.contains("join")){
                       System.out.println("Entrando sala : "+msg);
                  }else if(msg.contains("startGame")) {
               	   System.out.println("Empieza la partida : "+msg);
                  }
                 
          });
          c.connect("ws://127.0.0.1:9000/snake");
          Clientes[i] = c;
          
      } 
      
      for(WebSocketClient c : Clientes){
   	  
        new Thread(()->{
            try {
            	//Conectamos 4 jugadoes
           	 System.out.println(Unirest.post("http://127.0.0.1:9000/createPlayer").header("Content-Type", "application/json").body("{\"name\":\"Paquito"+Thread.currentThread().getName()+"\",\"difficulty\":0,\"gameMode\":0}").asJson().getBody());
           	 c.sendMessage("{\"instruction\": \"joinroom\", \"room\":"+sala.get()+", \"name\":\"Paquito"+Thread.currentThread().getName()+"\"}')");
           	 
            } catch (Exception e) {
                System.out.println(e);
            }
        }).start();   
        
        
      }
      
      //Espera a que se empiece la partida
      latch.await();
      

	}
}
