	package es.codeurjc.em.snake;

import java.util.ArrayDeque;
import java.util.Deque;

import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

public class Fruit {

	private Location position;
	
	private final int id;

	private final String hexColor;


	public Fruit(int id) {
		this.id = id;
		this.hexColor = "white";
		this.setPosition(SnakeUtils.getRandomLocation());
	}


	public Location getPosition() {
		return position;
	}


	public void setPosition(Location position) {
		this.position = position;
	}
	
	public String getColor() {
		return hexColor;
	}


	public int getId() {
		return id;
	}
	
}