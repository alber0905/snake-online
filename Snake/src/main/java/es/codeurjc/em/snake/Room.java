package es.codeurjc.em.snake;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.web.socket.WebSocketSession;

public class Room {
	private SnakeGame snakeGame;
	String name;
	int id;
	int difficulty;
	int gameMode;
	boolean genteEsperando;
	private AtomicInteger snakeIds = new AtomicInteger(0);
	private static final String SNAKE_ATT = "snake";
	
	public Room() {
		
	}
	
	public Room (String name, int dificulty, int gameMode) {
		this.genteEsperando = false;
		this.name = name;
		this.difficulty = dificulty;
		this.gameMode = gameMode;
		this.snakeGame = new SnakeGame(difficulty);
		this.id = -1;
	}
	
	public void initializeGame() {
		this.snakeGame = new SnakeGame(difficulty);
	}
	
	synchronized public int getId() {
			return id;
		
	}

	synchronized public void setId(int id) {
		this.id = id;
	}

	synchronized public String getName() {
		return name;
	}

	synchronized public void setName(String name) {
		this.name = name;
	}

	synchronized public int getDifficulty() {
		return difficulty;
	}

	synchronized public void setDifficulty(int dificulty) {
		this.difficulty = dificulty;
	}

	synchronized public int getGameMode() {
		return gameMode;
	}

	synchronized public void setGameMode(int gameMode) {
		this.gameMode = gameMode;
	}

	public int GetLastIdSala() {
		return snakeIds.getAndIncrement();
	}
	
	public synchronized int GetLastIdGame() {
		return this.snakeGame.getNumSnakes();
	}
	
	public synchronized void startTimer() {
		snakeGame.startTimer();
	}
	
	public synchronized void stopTimer() {
		snakeGame.stopTimer();
	}
	
	public void AddSnake(Snake s) {
		try {
			snakeGame.addSnake(s);
		}
		catch(Exception e) {
			System.out.println("Error al añadir una serpiente en la sala");
			e.printStackTrace();
		}
	}
	public void RemoveSnake(Snake s) {
		try {
			snakeGame.removeSnake(s);
		}catch(Exception e) {
			System.out.println("Error al eliminar una serpiente de la sala");
			e.printStackTrace();
		}
		
	}
	
	public Collection<Snake> getSnakes(){
		try {
			return snakeGame.getSnakes();
		}
		catch(Exception e) {
			System.out.println("Error al coger serpientes de la sala");
			e.printStackTrace();
			return null;
		}
	}
	
	synchronized public void broadcast(String message) {
		try {
			snakeGame.broadcast(message);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error al enviar un mensaje de todos las serpientes de la sala");
			e.printStackTrace();
		}
	}

}
