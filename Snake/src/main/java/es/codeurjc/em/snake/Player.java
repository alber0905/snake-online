package es.codeurjc.em.snake;

public class Player {
	int idPlayerSession;
	long timeJoin;
	
	public Player(int id) {
		idPlayerSession = id;
		timeJoin = System.currentTimeMillis();
	}
	
	public int GetId () {
		return idPlayerSession;
	}
	
	public boolean MoreThan5s() {
		long timeOnRoom = timeJoin - System.currentTimeMillis();
		return (timeOnRoom>5000);
	}
}
