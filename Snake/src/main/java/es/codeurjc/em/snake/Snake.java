package es.codeurjc.em.snake;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Deque;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

/*Clase de la serpiente*/
public class Snake {

	private static final int DEFAULT_LENGTH = 5;
	private long timeJoin;
	private final int id;
	private boolean enEspera;

	private Location head;
	private final Deque<Location> tail = new ArrayDeque<>();
	private int length = DEFAULT_LENGTH;

	private final String hexColor;
	private Direction direction;
	private int record;
	private String playerName;

	private final WebSocketSession session;

	public Snake(int id, String playerName, WebSocketSession session) {
		this.id = id;
		this.playerName = playerName;
		this.session = session;
		this.hexColor = SnakeUtils.getRandomHexColor();
		this.timeJoin = System.currentTimeMillis();
		resetState();
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	private void resetState() {
		this.direction = Direction.NONE;
		this.head = SnakeUtils.getRandomLocation();
		this.tail.clear();
		this.length = DEFAULT_LENGTH;
	}

	//Mensaje que se envia al morir
	private synchronized void kill() throws Exception {
		resetState();
		if(this.getRecord()>0)
		this.setRecord(this.getRecord() - 10);
		sendMessage(String.format("{\"type\": \"dead\", \"record\": %d}",this.getRecord()));
	}

	//Mensaje que se envia al matar otra serpiente
	private synchronized void reward() throws Exception {
		sendMessage("{\"type\": \"kill\"}");
	}
	
	//Mensaje que se envia al comernos una fruta
	private synchronized void eatFruit(int delet) throws Exception {
		this.length++;
		this.setRecord(this.getRecord() + 10);
		sendMessage(String.format("{\"type\": \"fruit\",\"id\" : %d, \"record\": %d}",delet,this.getRecord()));
	}
	
	//Comprobamos si nuestra cabeza coincide con la posición de alguna fruta
	public int handleFruits(Collection<Fruit> frutas) throws Exception {
		
		for(Fruit fruta : frutas) {
			if(this.getHead().equals(fruta.getPosition())) {
				eatFruit(fruta.getId());
				return fruta.getId();
			}
		}
		
		return -1;
		
	}

	protected synchronized void sendMessage(String msg) throws Exception {
		if(this.session.isOpen()) {
			this.session.sendMessage(new TextMessage(msg));
		}
	}

	public synchronized void update(Collection<Snake> snakes) throws Exception {

		Location nextLocation = this.head.getAdjacentLocation(this.direction);

		if (nextLocation.x >= Location.PLAYFIELD_WIDTH) {
			nextLocation.x = 0;
		}
		if (nextLocation.y >= Location.PLAYFIELD_HEIGHT) {
			nextLocation.y = 0;
		}
		if (nextLocation.x < 0) {
			nextLocation.x = Location.PLAYFIELD_WIDTH;
		}
		if (nextLocation.y < 0) {
			nextLocation.y = Location.PLAYFIELD_HEIGHT;
		}

		if (this.direction != Direction.NONE) {
			this.tail.addFirst(this.head);
			if (this.tail.size() > this.length) {
				this.tail.removeLast();
			}
			this.head = nextLocation;
		}

		handleCollisions(snakes);
	}

	private void handleCollisions(Collection<Snake> snakes) throws Exception {

		for (Snake snake : snakes) {

			boolean headCollision = this.id != snake.id && snake.getHead().equals(this.head);

			boolean tailCollision = snake.getTail().contains(this.head);

			if (headCollision || tailCollision) {
				kill();
				if (this.id != snake.id) {
					snake.reward();
				}
			}
		}
	}

	public synchronized Location getHead() {
		return this.head;
	}

	public synchronized Collection<Location> getTail() {
		return this.tail;
	}

	public synchronized void setDirection(Direction direction) {
		this.direction = direction;
	}

	public int getId() {
		return this.id;
	}

	public String getHexColor() {
		return this.hexColor;
	}
	
	public synchronized boolean MoreThan5s() {
		long timeOnRoom = System.currentTimeMillis()-timeJoin;
		return (timeOnRoom>5000);
	}
	
	public synchronized WebSocketSession GetSession() {
		return this.session;
	}

	public int getRecord() {
		return record;
	}

	public void setRecord(int record) {
		this.record = record;
	}
}
