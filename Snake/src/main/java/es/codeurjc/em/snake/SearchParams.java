package es.codeurjc.em.snake;

public class SearchParams {
	private int difficulty;
	private int gameMode;
	
	public SearchParams() {
		
	}

	public int getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(int difficulty) {
		this.difficulty = difficulty;
	}

	public int getGameMode() {
		return gameMode;
	}

	public void setGameMode(int gameMode) {
		this.gameMode = gameMode;
	}

	public SearchParams(int difficulty, int gameMode) {
		this.difficulty = difficulty;
		this.gameMode = gameMode;
	}
	
}
