package es.codeurjc.em.snake;

public class JSONRoom {

	private String name;
	private int difficulty;
	private int gameMode;
	
	public JSONRoom() {
		
	}
	
	public JSONRoom(String name, int difficulty, int gameMode) {
		super();
		this.name = name;
		this.difficulty = difficulty;
		this.gameMode = gameMode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getDifficulty() {
		return difficulty;
	}
	public void setDifficulty(int difficulty) {
		this.difficulty = difficulty;
	}
	public int getGameMode() {
		return gameMode;
	}
	public void setGameMode(int gameMode) {
		this.gameMode = gameMode;
	}
	
}
