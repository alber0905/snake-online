package es.codeurjc.em.snake;

import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.io.BufferedReader;
import java.util.concurrent.CopyOnWriteArrayList;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;



@RestController
public class SnakeHandler extends TextWebSocketHandler {

	private static final String SNAKE_ATT = "snake";
	private static final String ROOM_ATT = "room";
	private static final String PLAYER_ATT = "pName";

	private AtomicInteger snakeIds = new AtomicInteger(0);
	private AtomicInteger roomsIds = new AtomicInteger(0);
	private ObjectMapper mapper = new ObjectMapper();
	private SnakeGame snakeGame = new SnakeGame(0);
	
	private CopyOnWriteArrayList<String> allNames = new CopyOnWriteArrayList<>();
	private CopyOnWriteArrayList<String> allRoomNames = new CopyOnWriteArrayList<>();
	private Map<String, WebSocketSession> sessions = new ConcurrentHashMap<>();
	private Map<Integer, Room> rooms = new ConcurrentHashMap<Integer,Room>();
	private Map<Snake, Room> genteEsperando = new ConcurrentHashMap<Snake,Room>();
	
	private Lock insertSnake = new ReentrantLock();
	private Lock deleteSnake = new ReentrantLock();
	
	private ScheduledExecutorService scheduler;
	private static long MINIMUN_TICK_DELAY = 50;
	
	
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {

	}

	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {

		try {
			//Pasamos el mensaje a un arbol Json
			JsonNode node = mapper.readTree(message.getPayload());
			
			//Si el nodo instruccion tiene de valor ping hacemos un return
			if (node.get("instruction").asText().equals("ping")) {
				return;
			}
			//Si el nodo intruccion tiene el valor entrar en sala
			//Manejamos la entrada del cliente
			else if(node.get("instruction").asText().equals("joinroom")) {
				insertSnake.lock(); //La zona de unirse a una habitación está bajo exclusión mutua.
				System.out.println("JoinRoom");
				int indiceSala = node.get("room").asInt();			//Cogemos el indice de la sala a la que queremos entrar
				String playerName = node.get("name").asText();
				// Cuidado con concurrenciaaaaaa
				Room sala = rooms.get(indiceSala);					//Cogemos la sala según el índice, al ser concurrenthashmap no hay problema
				int id = sala.GetLastIdSala();
				Snake s = new Snake(id, playerName, session);					//Creamos una nueva serpiente
				int numGente = sala.GetLastIdGame();						//Cogemos el id de nuestro nuevo jugador, y lo incrementamos con AtomicInteger
				System.out.println("NumGente: " + numGente);
				if(id == 0) {
					startTimer();
				}
																	//Creamos una nueva serpiente
				
				if(numGente >= 4) { //Si la sala está llena
					System.out.println("Esperando");
					String msg = String.format("{\"type\": \"waiting\", \"id\":"+ id+"}"); //Mandamos un mensaje al cliente avisando de que empieza a esperar
					s.sendMessage(msg);
					genteEsperando.put(s, sala);
					
				}
				else {
					
					sala.AddSnake(s);									//Añadimos una serpiente, se añade al concurrentHashMap de snakes en SnakeGame
					
					session.getAttributes().put(SNAKE_ATT, s);			//Añadimos la serpiente como atributo de la sesión
					session.getAttributes().put(ROOM_ATT, indiceSala);
					session.getAttributes().put(PLAYER_ATT, playerName);
					
					StringBuilder sb = new StringBuilder();				//Pasamos la info de las despientes a los jugadores
					
					for (Snake snake : sala.getSnakes()) {			
					sb.append(String.format("{\"id\": %d, \"color\": \"%s\", \"name\":\"%s\"}", snake.getId(), snake.getHexColor(), snake.getPlayerName()));
						sb.append(',');
					}
					sb.deleteCharAt(sb.length()-1);
					String msg = String.format("{\"type\": \"join\",\"data\":[%s]}", sb.toString());
					
					sala.broadcast(msg);
				}
				insertSnake.unlock();
				
			}
			else {
				Snake s = (Snake) session.getAttributes().get(SNAKE_ATT);
				
				Direction d = Direction.valueOf(node.get("instruction").asText().toUpperCase());
				s.setDirection(d);
			}
			
		} catch (Exception e) {
			System.err.println("Exception processing message " + message.getPayload());
			e.printStackTrace(System.err);
		}
	}
	

	@Override
	synchronized public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		
		try {
			//Recuperamos la serpiente de la sesion
			Snake s = (Snake) session.getAttributes().get(SNAKE_ATT);
			//Si estaba en alguna sala la eliminamos y mandamos el mensaje a la sala
			if(session.getAttributes().containsKey(ROOM_ATT)) {
				int room_id = (int)session.getAttributes().get(ROOM_ATT);
				Room r = rooms.get(room_id);
				//Actualizamos el JSON de puntuaciones
				actualizarPuntuacion(new Puntuacion(s.getPlayerName(),s.getRecord()));
				r.RemoveSnake(s);
				String msg = String.format("{\"type\": \"leave\", \"id\": %d}", s.getId());
				//Comprobamos si al salir se ha vaciado la sala. Si es así, se elimina la sala
				if(r.GetLastIdGame()<=0) {
					r.stopTimer();
					rooms.remove(r.getId());
				}
				deleteSnake.lock();
				r.broadcast(msg);
				deleteSnake.unlock();
				
			}	
		}catch(Exception e) {
			System.out.print("Nuestro amigo el afterconnectionclose");
		}
		
	}
	
	//Función REST que inserta un nuevo jugador
	@PostMapping("/createPlayer")
    @ResponseStatus(HttpStatus.CREATED)
	public Jugador createPlayer(@RequestBody Jugador player) throws IOException {  
		System.out.println("New player: " + player.getName());
		boolean added = allNames.addIfAbsent(player.getName());
		player.setAdded(added);
		return player;
	}
	
	//Función REST para crear una sala si no existe
	@PostMapping("/createRoom")
    @ResponseStatus(HttpStatus.CREATED)
	public Room createRoom(@RequestBody Room room) throws IOException {
		if(allRoomNames.contains(room.getName())){
			System.out.println("Ya existe la sala");
			Room r = new Room();
			r.setId(-1); //Si existe la sala devolvemos una con id -1
			return r;
		}
		else {
			allRoomNames.add(room.name);
			int id = roomsIds.getAndIncrement();
			room.setId(id);
			room.initializeGame();
			rooms.put(id, room);
			System.out.println("Room created: :"+ room.getName());
			return room; //Si no existe la creamos, inicializamos y devolvemos
		}
	}
	
	//Función REST que devuelve el listado de habitaciones
	@PostMapping("/searchRooms")
    @ResponseStatus(HttpStatus.CREATED)
	public List<Room> createRoom(@RequestBody SearchParams params) throws IOException {  
		System.out.println("Buscando partidas...");
		List<Room> allRooms = new ArrayList<>();
		allRooms.addAll(rooms.values());		
		return allRooms;
	}
	
	//Función REST que devuelve una habitación con huecos libres
	@GetMapping("/matchmaking")
	@ResponseStatus(HttpStatus.CREATED)
	public Room matchMakingRoom() throws IOException{
		
		for(Room r : rooms.values()) {
			if(r.GetLastIdGame() <4) {
				System.out.println("Matchmaking returns: " + r.getName());
				return r;
			}
		}
		return null;
	}
	
	//Función REST para comenzar el juego. Se llama cuando el HOST pulsa el botón.
	@RequestMapping("/start/{id}")
	@ResponseBody
	public boolean startRoom(@PathVariable int id) throws IOException{
		System.out.println("Room id: " +id);
		Room r = rooms.get(id);
		if(r.GetLastIdGame()>0) {
			r.startTimer();
			String msg = String.format("{\"type\": \"startGame\"}");
			r.broadcast(msg);
			return true;
		}
		else {
			return false;
		}		
	}
	
	//Recogemos el JSON y actualizamos los datos con la lista de puntuaciones para capar en 10.
	public synchronized List<Puntuacion> actualizarPuntuacion(Puntuacion newPunt) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(System.getProperty("user.dir")+"/src/main/resources/static/json_files/Puntuacion.json"));
        
        Gson gson = new GsonBuilder().create();
        java.lang.reflect.Type listType = new TypeToken<List<Puntuacion>>(){}.getType();
        List<Puntuacion> puntuaciones = gson.fromJson(reader, listType);
        puntuaciones = newPunt.meterPuntuacion(puntuaciones);
        reader.close();
        
        FileWriter fileWriter = new FileWriter("./src/main/resources/static/json_files/Puntuacion.json");
        String jsonString = gson.toJson(puntuaciones);
        fileWriter.write(jsonString);
        fileWriter.close();
        
        
	return puntuaciones;
	}
    
	//Se recogen las puntuaciones para mostrarlas
    @GetMapping("/GetPuntuaciones")
    @ResponseStatus(HttpStatus.CREATED)
    public List<Puntuacion> getPuntuaciones() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("./src/main/resources/static/json_files/Puntuacion.json"));

        Gson gson = new GsonBuilder().create();
        java.lang.reflect.Type listType = new TypeToken<List<Puntuacion>>(){}.getType();
        List<Puntuacion> puntuaciones = gson.fromJson(reader, listType);

        reader.close();
	return puntuaciones;
    }
    
    //Función REST que se llama cuando un jugador cancela la espera para entrar a una sala
    @RequestMapping("/stopWait/{room_id}/{snake_id}")
	@ResponseBody
	public boolean stopWait(@PathVariable int room_id, @PathVariable int snake_id) throws IOException{
		for(Entry<Snake, Room> s: genteEsperando.entrySet()) {
			Snake sn = s.getKey();
			Room r = s.getValue();
			//Encontramos la entrada en el diccionario de gente esperando y la eliminamos
			if(sn.getId() == snake_id && r.getId() == room_id) {
				genteEsperando.remove(sn);
				System.out.println("Removed snake " + sn.getId() + " from room " + r.getId());
			}
		}
    	return true;
	}
    
    //Función que controla que cuando un jugador se queda esperando 5 segundos a entrar
	public void JoinRoomOrLeave() {
		for(Entry<Snake,Room> s : genteEsperando.entrySet()) {
			Snake sn = s.getKey();
			if(sn.MoreThan5s()) {				
				genteEsperando.remove(sn);
				String msg = String.format("{\"type\": \"stopWait\", \"name\": \"%s\"}", sn.getPlayerName());
				try {
					if(!scheduler.isShutdown() && !scheduler.isTerminated()) {
						sn.sendMessage(msg);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				}
			else {
				if(s.getValue().GetLastIdGame()<4) {
					Room sala = s.getValue();
					sala.AddSnake(sn);									//Añadimos una serpiente, se añade al concurrentHashMap de snakes en SnakeGame
					WebSocketSession session = sn.GetSession(); 
					session.getAttributes().put(SNAKE_ATT, sn);			//Añadimos la serpiente como atributo de la sesión
					session.getAttributes().put(ROOM_ATT, sala.getId());
					session.getAttributes().put(PLAYER_ATT, sn.getPlayerName());
					
					StringBuilder sb = new StringBuilder();				//Pasamos la info de las despientes a los jugadores
					
					for (Snake snake : sala.getSnakes()) {			
						sb.append(String.format("{\"id\": %d, \"color\": \"%s\", \"name\":\"%s\"}", snake.getId(), snake.getHexColor(), snake.getPlayerName()));
							sb.append(',');
						}
					sb.deleteCharAt(sb.length()-1);
					String msg = String.format("{\"type\": \"join\",\"data\":[%s]}", sb.toString());
					if(!scheduler.isShutdown() && !scheduler.isTerminated()) {
						sala.broadcast(msg);
					}
					genteEsperando.remove(sn);
					
				}
			}
		}
	}
	
	//Creamos un Executor para ejecutar la función que controla los jugadores en cola para entrar en la partida
	public void startTimer() {
		scheduler = Executors.newScheduledThreadPool(1);
		scheduler.scheduleAtFixedRate(() -> JoinRoomOrLeave(), MINIMUN_TICK_DELAY, MINIMUN_TICK_DELAY, TimeUnit.MILLISECONDS);
	}
	
	//Paramos el scheduler
	public void stopTimer() {
		if (scheduler != null) {
			scheduler.shutdownNow();
		}
	}
	

}

