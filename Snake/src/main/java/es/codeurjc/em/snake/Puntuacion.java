package es.codeurjc.em.snake;

import java.util.Collections;
import java.util.List;

public class Puntuacion implements Comparable<Puntuacion> {	
	
	private int puntuacion;
	private String nombre;

	public Puntuacion() {
	}
	
	public Puntuacion(String nombre,int puntuacion) {
		this.puntuacion = puntuacion;
		this.nombre = nombre;
	}
	
	public String getName() {
		return nombre;
	}
	
	public void setName(String name) {
		this.nombre = name;
	}
	
	public int getPunt() {
		return puntuacion;
	}
	
	public void setPunt(int punt) {
		this.puntuacion = punt;
	}
	
	    public List<Puntuacion> meterPuntuacion(List<Puntuacion> listaPuntuaciones){
	        int i = 0;
	        
	      //Comprobamos que al meter una nueva puntuación no exista el usuario, o que si existe tenga una puntuación
	        //menor en la tabla de la que ha obtenido
	        boolean introducir=true;
	        for(Puntuacion clasificacion :listaPuntuaciones) {
	        	if(clasificacion.getName().equals(this.getName())) {
	        		if(clasificacion.getPunt()>=this.puntuacion) {
	        			introducir = false;
	        	}else {
	        		introducir=false;
	        		clasificacion.setPunt(this.puntuacion);
	        	}
	        		
	        	}
	        }

	        while(i < listaPuntuaciones.size() && this.puntuacion < listaPuntuaciones.get(i).getPunt()){
	            i++;
	        }
	        //Si no se repite, añadimos la puntuacion
	        if(introducir)
	        listaPuntuaciones.add(i, this);
	        //Acotamos la lista a 10
	        if(listaPuntuaciones.size()>10){
	            listaPuntuaciones.remove(10);
	        }
	        //Ordenamos la lista de mayor a menor
	        Collections.sort(listaPuntuaciones);
	        return listaPuntuaciones;
	    }

		@Override
		public int compareTo(Puntuacion o) {
			if (puntuacion < o.getPunt()) {
                return 1;
            }
            if (puntuacion > o.getPunt()) {
                return -1;
            }
            return 0;
		}

}
