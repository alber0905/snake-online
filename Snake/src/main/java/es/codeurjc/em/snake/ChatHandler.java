package es.codeurjc.em.snake;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

@RestController
public class ChatHandler extends TextWebSocketHandler {

	private static final String PLAYER_ATT = "playerName";
	private Map<String, WebSocketSession> sessions = new ConcurrentHashMap<>();
	private ObjectMapper mapper = new ObjectMapper();
	
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		System.out.println("New user: " + session.getId());
		sessions.put(session.getId(), session);
	}
	
	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		System.out.println("Session closed: " + session.getId());
		sendOtherParticipants(session, session.getAttributes().get(PLAYER_ATT) + " ha abandonado la sala", true);
		sessions.remove(session.getId());
	}
	
	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
		
		JsonNode node = mapper.readTree(message.getPayload());
		
		if(node.get("instruction").asText().equals("registerName")) {
			String name = node.get("name").asText();
			session.getAttributes().put(PLAYER_ATT, name);
			sendOtherParticipants(session, name + " se ha unido a la sala", true);
			
		}
		else if(node.get("instruction").asText().equals("message")) {
			sendOtherParticipants(session, node.get("message").asText(), false);
		}
	}

	private void sendOtherParticipants(WebSocketSession session, String msg, boolean chatBot) throws IOException {

		System.out.println("Message sent: " + msg);
		
		ObjectNode newNode = mapper.createObjectNode();
		if(!chatBot) {
			newNode.put("name", (String)session.getAttributes().get(PLAYER_ATT));
		}
		else {
			newNode.put("name", "ChatBot");
		}
		newNode.put("message", msg);
		
		
		for(WebSocketSession participant : sessions.values()) {
			if(!participant.getId().equals(session.getId())) {
				participant.sendMessage(new TextMessage(newNode.toString()));
			}
		}
	}
	
	@GetMapping("/GetChatUsers")
    @ResponseStatus(HttpStatus.CREATED)
    public List<String> getUsers() throws IOException {
		List<String> names = new ArrayList<>();
		for(WebSocketSession participant : sessions.values()) {
			names.add((String)participant.getAttributes().get(PLAYER_ATT));
		}
        return names;
    }

}
