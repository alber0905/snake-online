	package es.codeurjc.em.snake;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

/*Clase que crea el servidor*/
@SpringBootApplication
@EnableWebSocket
public class Application implements WebSocketConfigurer {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
		registry.addHandler(snakeHandler(), "/snake").setAllowedOrigins("*");
		registry.addHandler(chatHandler(), "/chat").setAllowedOrigins("*");
	}

	@Bean
	public WebSocketHandler snakeHandler() {
		return new SnakeHandler();
	}
	
	@Bean
	public WebSocketHandler chatHandler() {
		return new ChatHandler();
	}
}
