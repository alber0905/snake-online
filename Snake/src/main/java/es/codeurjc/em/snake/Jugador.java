package es.codeurjc.em.snake;

public class Jugador {

	private String name;
	private boolean added = false;
	
	public Jugador() {
		
	}
	
	public Jugador(String name) {
		this.name = name;
	}

	public boolean isAdded() {
		return added;
	}

	public void setAdded(boolean added) {
		this.added = added;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
