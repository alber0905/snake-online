package es.codeurjc.em.snake;

import java.util.List;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SnakeGame {

	private static long TICK_DELAY;
	private static long fruitTime;
	
	private ConcurrentHashMap<Integer, Snake> snakes = new ConcurrentHashMap<>();
	private ConcurrentHashMap<Integer, Fruit> fruits = new ConcurrentHashMap<>();
	private AtomicInteger numSnakes = new AtomicInteger();
	private AtomicInteger tiempoFruta = new AtomicInteger();
	private AtomicInteger idFruta = new AtomicInteger();
	private AtomicBoolean isTimerStarted = new AtomicBoolean(false);

	private ScheduledExecutorService scheduler;
	
	public SnakeGame(int difficulty) {
		switch(difficulty){
		case 0:
			TICK_DELAY = 150;
			fruitTime = 30;
			break;
		case 1:
			TICK_DELAY = 100;
			fruitTime = 30;
			break;
		case 2:
			TICK_DELAY = 50;
			fruitTime = 30;
			break;
		default:
			break;
		}
	}
	
	//Función que añade una serpiente al juego
	public void addSnake(Snake snake) {

		snakes.put(snake.getId(), snake);

		int count = numSnakes.getAndIncrement();

		if(count == 1) {
			//Si es el segundo jugador, mandamos el mensaje para que el host muestre el boton de empezar
			String msg = String.format("{\"type\": \"showStartButton\"}");
			List<Snake> snakesValues = new ArrayList<>();
			snakesValues.addAll(snakes.values());
			try {
				//El host es la primera serpiente que se añadió.
				snakesValues.get(0).sendMessage(msg);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		//Si llenamos la sala empezamos a contar el tiempo
		if (count == 3) {
			startTimer();
		}
		
		//En cuanto empieza a contar el tiempo mandamos mensaje de comenzar juego
		if(isTimerStarted.get()) {
			String msg = String.format("{\"type\": \"startGame\"}");
			try {
				broadcast(msg);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	//Devuelve las serpientes del juego
	public Collection<Snake> getSnakes() {
		return snakes.values();
	}

	//Elimina una serpiente del juego
	public synchronized void removeSnake(Snake snake) {
		snakes.remove(Integer.valueOf(snake.getId()));

		int count = numSnakes.decrementAndGet();
		
		//Si solo queda una serpiente y el juego ya se había iniciado, terminamos la partida
		if (count == 1 && (isTimerStarted.get())) {
			stopTimer();
		}
	}

	//Funcion que controla la partida
	private void tick() {
			
			int time = tiempoFruta.getAndIncrement();
			try {
				//Actualizamos posiciones de las serpientes y comprobamos si se ha comido una fruta
				for (Snake snake : getSnakes()) {
					snake.update(getSnakes());
					int indice = snake.handleFruits(fruits.values());
					//Si se come una fruta, se elimina de la lista de frutas y se actualiza la posición en todos los clientes
					if(indice!=-1) {
						fruits.remove(indice);
						if(!scheduler.isShutdown() && !scheduler.isTerminated()) {
						broadcast(String.format("{\"type\": \"deleteFruit\",\"id\" : %d}",indice));
						}
					}
				}
				
				//Obtenemos todas las posiciones de todas las serpientes en juego
				StringBuilder sb = new StringBuilder();
				for (Snake snake : getSnakes()) {
					sb.append(getLocationsJson(snake));
					sb.append(',');
				}
				//Eliminamos la coma del final
				if(sb.length()!=0)
				sb.deleteCharAt(sb.length()-1);
					//Actualizamos posiciones en todos los clientes
					String msg = String.format("{\"type\": \"update\", \"data\" : [%s]}", sb.toString());
				if(!scheduler.isShutdown() && !scheduler.isTerminated()) {
					broadcast(msg);
				}
				//Si hay frutas almacenadas, actualizamos sus posiciones por si se hubiese generado alguna nueva
				if(!fruits.isEmpty()) {
					StringBuilder updF = new StringBuilder();
					//Devolvemos las posiciones de todas las frutas
					for(Fruit fruta : fruits.values()) {
						updF.append(String.format("{\"id\": %d, \"pos\": [{\"x\": %d, \"y\": %d}]}",fruta.getId(), fruta.getPosition().x, fruta.getPosition().y));
						updF.append(',');
					}
					//Eliminamos la coma del final y actualizams las frutas
					updF.deleteCharAt(updF.length()-1);
					String updFruta = String.format("{\"type\": \"updateFruits\", \"data\" : [%s]}", updF.toString());
					if(!scheduler.isShutdown() && !scheduler.isTerminated()) {
						broadcast(updFruta);
					}
				}
				
				//GENERADOR DE FRUTAS
				if(time >=fruitTime) {
					//Añadimos fruta a la lista
					fruits.put(idFruta.get(),new Fruit(idFruta.get()));
					StringBuilder sbFrutas = new StringBuilder();
					//Cogemos la última fruta
					sbFrutas.append(String.format("{\"id\": %d, \"color\" : \" %s \"}",idFruta.get(), fruits.get(idFruta.get()).getColor()));
					tiempoFruta.set(0);
					//Aumentamos en uno el id de la fruta para evitar repeticiones y spawneamos la fruta en el tablero
					idFruta.getAndIncrement();
					String enviarFruta = String.format("{\"type\": \"spawnFruit\", \"data\" : [%s]}", sbFrutas.toString());
					//Si el juego sigue en marcha, se envia el mensaje
					if(!scheduler.isShutdown() && !scheduler.isTerminated()) {
					broadcast(enviarFruta);
					}
					
				}
				//Comprobamos si alguien ha llegado a la puntuación máxima
				for(Snake sn: getSnakes()) {
					if(sn.getRecord()==100) {
						stopTimer();
					}
				}
	
			} catch (Throwable ex) {
				System.err.println("Exception processing tick()");
				ex.printStackTrace(System.err);
			}
			
		
	}
	
	//Se recorren todas las serpientes y se devuelve la nueva posición del cuerpo junto con la puntuación actualizada
	private String getLocationsJson(Snake snake) {

		synchronized (snake) {
			//Almacenamos la posicion de la cabeza
			StringBuilder sb = new StringBuilder();
			sb.append(String.format("{\"x\": %d, \"y\": %d}", snake.getHead().x, snake.getHead().y));
			//Almacenamos la posición del cuerpo
			for (Location location : snake.getTail()) {
				sb.append(",");
				sb.append(String.format("{\"x\": %d, \"y\": %d}", location.x, location.y));
			}
			
			
			//Devolvemos la posición de la serpiente
			return String.format("{\"id\":%d,\"body\":[%s], \"record\": %d}", snake.getId(), sb.toString(),snake.getRecord());
		}
	}

	//Función sincronizada utilizada para enviar mensajes a todas las serpientes
	synchronized public void broadcast(String message) throws Exception {
		for (Snake snake : getSnakes()) {
			try {

				snake.sendMessage(message);

			} catch (Throwable ex) {
				System.err.println("Execption sending message to snake " + snake.getId());
				ex.printStackTrace(System.err);
				removeSnake(snake);
			}
		}
	}

	//Executor que repite la actualización del juego
	public void startTimer() {
		if(!isTimerStarted.get()) {
			isTimerStarted.set(true);
			scheduler = Executors.newScheduledThreadPool(1);
			scheduler.scheduleAtFixedRate(() -> tick(), TICK_DELAY, TICK_DELAY, TimeUnit.MILLISECONDS);
		}
	}

	public void stopTimer() {
		//Cuando la partida ha terminado, se avisa a todos y se redirecciona a la tabla de puntuaciones
		for(Snake snake : getSnakes()) {
			try {
				//Finalizamos la partida y redireccionamos a los clientes
				String msg = String.format("{\"type\": \"finish\", \"name\": \"%s\"}",snake.getPlayerName());
				snake.sendMessage(msg);
			}catch(Throwable e) {
				
			}
			
		}
		if (scheduler != null) {
			scheduler.shutdownNow();
		}
		
		
	}
	
	
	public synchronized int getNumSnakes() {
		return numSnakes.get();
	}
}

