var Console = {};

Console.log = (function(message) {
	var console = document.getElementById('console');
	var p = document.createElement('p');
	p.style.wordWrap = 'break-word';
	p.innerHTML = message;
	console.appendChild(p);
	while (console.childNodes.length > 25) {
		console.removeChild(console.firstChild);
	}
	console.scrollTop = console.scrollHeight;
});

let game;

class Snake {

	constructor() {
		this.snakeBody = [];
		this.color = null;
	}

	draw(context) {
		for (var pos of this.snakeBody) {
			context.fillStyle = this.color;
			context.fillRect(pos.x, pos.y,
				game.gridSize, game.gridSize);
		}
	}
}

class Fruit {

	constructor() {
		this.fruitPos = [];
		this.fruitColor = 'white';
		this.record = 0;
	}

	draw(context) {
		for (var pos of this.fruitPos) {
			context.fillStyle = this.fruitColor;
			context.fillRect(pos.x, pos.y,
				game.gridSize, game.gridSize);
		}
	}
}

class Game {

	constructor(){
	
		this.fps = 30;
		this.socket = null;
		this.nextFrame = null;
		this.interval = null;
		this.direction = 'none';
		this.gridSize = 10;		
		this.skipTicks = 1000 / this.fps;
		this.nextGameTick = (new Date).getTime();
	}

	//initialice recibe el nombre del jugador y el id de la sala a la que se mete el jugador 
	initialize(roomId, playerName) {	
	
		this.snakes = [];
		this.fruits = [];
		let canvas = document.getElementById('playground');
		if (!canvas.getContext) {
			Console.log('Error: 2d canvas not supported by this browser.');
			return;
		}
		
		this.context = canvas.getContext('2d');
		window.addEventListener('keydown', e => {
			
			var code = e.keyCode;
			if (code > 36 && code < 41) {
				switch (code) {
				case 37:
					if (this.direction != 'east')
						this.setDirection('west');
					break;
				case 38:
					if (this.direction != 'south')
						this.setDirection('north');
					break;
				case 39:
					if (this.direction != 'west')
						this.setDirection('east');
					break;
				case 40:
					if (this.direction != 'north')
						this.setDirection('south');
					break;
				}
			}
		}, false);
		
		//Se pasan por parámetro a connect
		this.connect(roomId, playerName);
	}

	setDirection(direction) {
		this.direction = direction;
		this.socket.send('{"instruction":"'+direction+ '"}');
		Console.log('Sent: Direction ' + direction);
	}

	startGameLoop() {
	
		this.nextFrame = () => {
			requestAnimationFrame(() => this.run());
		}
		
		this.nextFrame();		
	}

	stopGameLoop() {
		this.nextFrame = null;
		if (this.interval != null) {
			clearInterval(this.interval);
		}
	}

	draw() {
		this.context.clearRect(0, 0, 640, 480);
		for (var id in this.snakes) {			
			this.snakes[id].draw(this.context);
		}
		for (var id in this.fruits) {			
			this.fruits[id].draw(this.context);
		}
	}

	addSnake(id, color) {
		this.snakes[id] = new Snake();
		this.snakes[id].color = color;
	}

	updateSnake(id, snakeBody, record) {
		if (this.snakes[id]) {
			this.snakes[id].snakeBody = snakeBody;
			this.snakes[id].record = record;
		}
	}

	removeSnake(id) {
		this.snakes[id] = null;
		// Force GC.
		delete this.snakes[id];
	}

	run() {
	
		while ((new Date).getTime() > this.nextGameTick) {
			this.nextGameTick += this.skipTicks;
		}
		this.draw();
		if (this.nextFrame != null) {
			this.nextFrame();
		}
	}

	connect(roomId, playerName) {
		
		this.socket = new WebSocket('ws://'+window.location.host+'/snake');

		this.socket.onopen = () => {
			
			// Socket open.. start the game loop.
			Console.log('Info: WebSocket connection opened.');
			//Al abrir el socket se manda una llamada al socket para que asocie la sala y el nombre a la sesión
			this.socket.send('{"instruction": "joinroom", "room":' + roomId + ', "name":"' + playerName +'"}');		
			
		}

		this.socket.onclose = () => {
			Console.log('Info: WebSocket closed.');
			this.stopGameLoop();
		}

		this.socket.onmessage = (message) => {

			var packet = JSON.parse(message.data);
			
			switch (packet.type) {
			case 'update':
				for (var i = 0; i < packet.data.length; i++) {
					this.updateSnake(packet.data[i].id, packet.data[i].body, packet.data[i].record);
					$(".jqPlayerInfo_" + packet.data[i].id + " .jqPunt").text(packet.data[i].record);
				}
				break;
			case 'join':
				for (var j = 0; j < packet.data.length; j++) {
					if($(".jqPlayerInfo_" + packet.data[j].id).length == 0){
						this.addSnake(packet.data[j].id, packet.data[j].color);
						//Cuando se une un jugador rellenamos su plantilla con nombre y color
						var text = $("#snakes_template").html();
						text = text.replace("%PLAYER_NAME%", packet.data[j].id);
						text= text.replace("%COLOR%", packet.data[j].color);
						text= text.replace("%NAME%", packet.data[j].name);
						$(".color_container").append(text);
					}					 
				}
				break;
			case 'leave':
				this.removeSnake(packet.id);
				//Quitamos el div con la info del jugador
				$(".jqPlayerInfo_" + packet.id).remove();
				break;
			case 'dead':
				//Morimos por comernos a nosotros o intentar comer otra serpiente
				Console.log('Info: Your snake is dead, bad luck!');
				Console.log('Info: Record:'+packet.record);
				this.direction = 'none';
				break;
			case 'kill':
				//Alguien se choca con nosotros y muere
				Console.log('Info: Head shot!');
				break;
			case 'fruit':
				//Nos comemos una fruta y aumentamos la puntuación
				Console.log('Info: Ñam Ñam!');
				Console.log('Info: Record:'+packet.record);
				break;
			case 'spawnFruit':
				//Creamos una fruta nueva
				this.addFruit(packet.data[0].id,packet.data[0].color);
				break;
			case 'updateFruits' :
				//Actualizamos las posiciones de las frutas por si se añade una o se elimina
				for (var i = 0; i < packet.data.length; i++) {
					this.updateFruits(packet.data[i].id, packet.data[i].pos);
				}
				break;
			case 'deleteFruit':
				//Si una serpiente se ha comido una fruta, la eliminamos
				this.removeFruit(packet.id);
				break;
			case 'startGame':
				//Eliminamos el botón de empezar juego (solo en el host)
				$(".jqComenzarActive").remove();
				Console.log("Info: Game started.");
				Console.log('Info: Press an arrow key to begin.');		
				//Esto se hacía en el socket.onopen, pero ahora hay que hacerlo aquí
				this.startGameLoop();			
				setInterval(() => this.socket.send('{"instruction":"ping"}', 5000));
				break;
			case 'showStartButton':
				//Mostramos y activamos el botón de comenzar (esto solo se llama si somos host)
				$(".jqComenzar").removeClass("hidden");
				$(".jqComenzar").addClass("block");
				$(".jqComenzar").addClass("jqComenzarActive");
				$(".jqComenzar").removeClass("jqComenzar");
				break;
			case 'waiting':
				//Si estamos esperando a entrar a una sala activamos el botón de cancelar la espera
				$(".jqCancelar").removeClass("hidden");
				$(".jqCancelar").addClass("block");
				$(".jqCancelar").addClass("jqCancelarActive");
				$(".jqCancelar").removeClass("jqCancelar");
				snakeId = packet.id;
				break;
			case 'stopWait':
				//Si hemos cancelado la espera, tras comprobar con el servidor se redirige al menú
				window.location.replace("main_menu.html?name="+packet.name);
				break;
			case 'finish':
				//Si solo queda una serpiente en juego o se ha alcanzado la puntuacion máxima, cerramos sala y redireccionamos a la leaderboard
				Console.log('Fin de Partida!');
				window.location.replace('leaderboard.html?name='+packet.name);
				break;

			default:
				Console.log('Unknown Message');
				break;
			}
			
		}
	}

	//Añadimos una nueva fruta
	addFruit(id,color) {
		this.fruits[id] = new Fruit();
		this.fruits[id].fruitColor = color;
	}
	
	//Eliminamos la fruta cuando se la come una serpiente
	removeFruit(id) {

		this.fruits[id] = null;
		// Force GC.
		delete this.fruits[id];
	}
	
	//Actualizamos las posiciones de las frutas por si ha aparecido otra o se la han comido
	updateFruits(id, pos) {
		if (this.fruits[id]) {
			this.fruits[id].fruitPos = pos;
		}else{
			this.fruits[id] = new Fruit();
		}
	}
	
}

var snakeId = -1;
game = new Game();
$(document).ready(function(){	
	//Cogemos la habitación y el nombre de la url, e inicializamos el juego
	var url = new URL(window.location.href);
	var room = url.searchParams.get("room");
	var name = url.searchParams.get("name");	
	game.initialize(room, name);

	//Funcionalidad del botón comenzar juego (HOST)
	$(document).on("click", ".jqComenzarActive", function(){
		$.ajax({
			method: "GET",
            url:"/start/" + room 
        }).done(function(data){
        	if(data){
        		$(".jqComenzarActive").remove();
        	}
        	else{
        		alert("No hay suficientes jugadores para empezar");
        	}
		});
	});
	
	//Funcionalidad del método cancelar espera
	$(document).on("click", ".jqCancelarActive", function(){
		$.ajax({
			method:"GET",
			url:"/stopWait/"+room + "/" + snakeId
		}).done(function(data){
			if(data){
				$(".jqCancelarActive").remove();
				window.location.replace("main_menu.html?name="+ name);
			}
		});
	});
});
