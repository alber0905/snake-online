$(document).ready(function(){
	if($("#player_name").length>0){
		//Recuperamos el nombre del jugador
		var url = new URL(window.location.href);
		var player_name = url.searchParams.get("name");
		$("#player_name").val(player_name);
	}
	//Buscamos todas las salas
	search(0, 0);
	setInterval(() => search(0, 0), 1000);
	
	//Al pinchar en una sala entramos en ella
	$(document).on('click', '.jq_start_game', function(){
		var id = $(this).data('room_id');
		window.location.replace("game.html?room="+id + "&name="+ $("#player_name").val());
	});
});

//search lanza una petición al servidor filtrando por dificultad y modo de juego, aunque no ha sido posible 
//implementar los filtros
function search(difficulty, gameMode){
	var data = JSON.stringify({"difficulty": difficulty, "gameMode":gameMode});
	$.ajax({
            method: "POST",
            url:"/searchRooms/",                
            contentType:"application/json",
            dataType:"json",
            data: data
        }).done(function(data){              
            $(".jqRooms").html('');
            var text = '';
            var totalText = '';
            for(var i = 0; i<data.length; i++){
            	//Rellenamos la tabla de salas
            	text = $("#room_template").html();
            	text = text.replace('%ID%', data[i].id);
            	text = text.replace('%NAME%', data[i].name);
            	text = text.replace('%PLAYERS%', data[i].snakes.length);
            	switch(data[i].difficulty){
            		case 0:
            			text = text.replace('%DIFFICULTY%', 'Easy');
            			break;
            		case 1:
            			text = text.replace('%DIFFICULTY%', 'Normal');
            			break;
            		case 2:
            			text = text.replace('%DIFFICULTY%', 'Hard');
            			break;
        			default:
        				text = text.replace('%DIFFICULTY%', 'IO K SE');
        				break;
            	}
            	totalText+= text;            	
            }
            
            $(".jqRooms").html(totalText);
        });
}