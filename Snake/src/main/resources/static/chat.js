$(document).ready(function(){
	//Creamos el socket del chat
	var socket = new WebSocket('ws://'+window.location.host+'/chat');
	var playerName = $("#player_name").val(); //Recogemos el nombre del jugador
	socket.onopen = () => {		
		console.log('Info: Chat WebSocket connected');		
		socket.send('{"instruction": "registerName", "name":"' + playerName +'"}'); //Registramos el nombre
		getUsers(); //Pedimos el listado de usuarios
		setInterval(() => getUsers(), 1000);
		
	}
	
	//Al recibir un mensaje lo introducimos en el div del chat
	socket.onmessage = function(msg) {
		console.log("WS message: " + msg.data);
		var message = JSON.parse(msg.data)
		var template = $("#text_message").html().replace("%NAME%", message.name).replace("%TEXT%", message.message);
		$('#chat').append(template);
	}
	
	//Enviamos un mensaje
	$("#chatForm").on("submit", function(e){
		e.preventDefault();
		var message = $("#message").val();	
		$("#message").val('');
		var msg = JSON.stringify({"instruction": "message", "message": message});
		
		var template = $("#text_message").html().replace("%NAME%", playerName).replace("%TEXT%", message);
		$('#chat').append(template);
		socket.send(msg);
	});
	
	//getUsers pide los usuarios conectados al chat y los mete en su div
	function getUsers(){
		$.ajax({
			method: "GET",
	        url:"/GetChatUsers/"
		}).done(function(data){
			$("#user_names").text('');
			var text = '';
			for(var i = 0; i<data.length; i++){
				text += data[i];
				if(i<data.length-1){
					text+= ", "
				}
			}
			
			$("#user_names").text(text);
		});
	}
});