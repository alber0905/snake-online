$(document).ready(function(){
	//recogemos el nombre del jugador
	var url = new URL(window.location.href);
	var player_name = url.searchParams.get("name");
    var htmlRet = "";
    $.ajax({ //pedimos las puntuaciones
        method: "GET",
        url:"/GetPuntuaciones/",                
        contentType:"application/json",
        dataType:"json"            
        }).done(function(data){
        	//Insertamos cada puntuación en la página
            for(var i = 0; i<data.length; i++){
                var template = $(".jqPuntTemplate").html();
                template = template.replace("%INDEX%", i+1 + ".");
                template = template.replace("%PUNT%", data[i].punt);
                template = template.replace("%NAME%", data[i].name);
                htmlRet += template;
            }
            $(".punt_break").append(htmlRet); 
    });
    
    //Botón para volver al menú 
    $(".leaderboar_btn").on("click", function(e){
    	e.preventDefault();
    	window.location.replace("main_menu.html?name=" + player_name);
    })
});