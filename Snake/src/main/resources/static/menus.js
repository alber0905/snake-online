$(document).ready(function(){
	//Formulario para registrar el nombre de usuario
	$("#name_form").on('submit', function(e){
		e.preventDefault();
		var data = JSON.stringify({"name": $("#player_name").val()});
		$.ajax({
            method: "POST",
            url:"/createPlayer/",                
            contentType:"application/json",
            dataType:"json",
            data: data
        }).done(function(data){              
            var name = data.name;
            var added = data.added;
            if(added){ //Si el nombre estaba disponible avanzamos
            	window.location.replace("main_menu.html?name="+data.name);
            }
            else{ //Si no mostramos un error
            	$(".jqError").text("El nombre que has elegido no está disponible");
            }
        }); 
	});
	
	//Formulario para crear una habitación
	$("#room_form").on('submit', function(e){
		e.preventDefault();
		var data = JSON.stringify({"name": $("#room_name").val(),
									"difficulty": $("#diff option:selected").val(),
									"gameMode":"0"}); //El gamemode sirve para poder ampliar varios modos de juego aunque ahora solo haya uno
		$.ajax({
            method: "POST",
            url:"/createRoom/",                
            contentType:"application/json",
            dataType:"json",
            data: data
        }).done(function(data){
        	if(data.id != -1){ //Si el id es != -1 no hay sala con este nombre, y se ha creado.
        		var id = data.id;            
            	window.location.replace("game.html?room="+id + "&name=" + $("#player_name").val());
        	}
        	else{
        		alert("Ya existe una sala con ese nombre");
        	}
        }); 
	});
	
	//Matchmaking para pedir una sala libre
	$(".jqMatchmaking").on("click", function(){
		$.ajax({
			method:"GET",
			url: "/matchmaking/",
			contentType:"application/json"
		}).done(function(data){
			if(data){
				window.location.replace("game.html?room="+data.id + "&name="+ $("#player_name").val());
			}
			else{
				alert("No hay salas disponibles en este momento");
			}
		});
	});
	
	if($("#player_name").length>0){
		var url = new URL(window.location.href);
		var player_name = url.searchParams.get("name");
		$("#player_name").val(player_name);
	}
	
	$(".jqRedirect").on('click', function(){
		var url = $(this).data("url");
		$(".replace_div").load(url);
		//window.location.replace($(this).data("url") + "?name=" + $("#player_name").val());
	});
	if($(".jqLeaderBoard").length>0){
	    var htmlRet = "";
	    $.ajax({
	        method: "GET",
	        url:"/GetPuntuaciones/",                
	        contentType:"application/json",
	        dataType:"json"            
	        }).done(function(data){
	            for(var i = 0; i<data.length; i++){
	                var template = $(".jqPuntTemplate").html();
	                template = template.replace("%INDEX%", i+1 + ".");
	                template = template.replace("%PUNT%", data[i].punt);
	                template = template.replace("%NAME%", data[i].name);
	                htmlRet += template;
	            }
	            $(".punt_break").after(htmlRet); 
	    });
	}
});



